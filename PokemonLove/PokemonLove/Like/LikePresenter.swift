//
//  LikePresenter.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 23/07/21.
//  
//

import Foundation

class LikePresenter  {
    
    // MARK: Properties
    weak var view: LikeViewProtocol?
    var interactor: LikeInteractorInputProtocol?
    var wireFrame: LikeWireFrameProtocol?
    
}

extension LikePresenter: LikePresenterProtocol {
    // TODO: implement presenter methods
    func viewDidLoad() {
        
        interactor?.getDataPokemonRamdonInteractor()

    }
    
    func likeAction(name: String, img: String, appDelegate: AppDelegate ){
        interactor?.likeAction(name: name, img: img, appDelegate: appDelegate)
    }
}

extension LikePresenter: LikeInteractorOutputProtocol {
    
    // TODO: implement interactor output methods
    
    func pushPokemonRamdonDetailPresenter(dataPokemonDetail: DetailPokemon){
        view?.presenterPushPokemonRamdonDetail(dataPokemonDetail: dataPokemonDetail)
    }
    
}
