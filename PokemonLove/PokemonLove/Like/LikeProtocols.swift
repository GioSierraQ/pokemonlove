//
//  LikeProtocols.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 23/07/21.
//  
//

import Foundation
import UIKit

protocol LikeViewProtocol: class {
    // PRESENTER -> VIEW
    var presenter: LikePresenterProtocol? { get set }
    
    func presenterPushPokemonRamdonDetail(dataPokemonDetail: DetailPokemon)
}

protocol LikeWireFrameProtocol: class {
    // PRESENTER -> WIREFRAME
    static func createLikeModule() -> UIViewController
}

protocol LikePresenterProtocol: class {
    // VIEW -> PRESENTER
    var view: LikeViewProtocol? { get set }
    var interactor: LikeInteractorInputProtocol? { get set }
    var wireFrame: LikeWireFrameProtocol? { get set }
    
    func viewDidLoad()
    
    func likeAction(name: String, img: String, appDelegate: AppDelegate )
}

protocol LikeInteractorOutputProtocol: class {
// INTERACTOR -> PRESENTER
    
    func pushPokemonRamdonDetailPresenter(dataPokemonDetail: DetailPokemon)
}

protocol LikeInteractorInputProtocol: class {
    // PRESENTER -> INTERACTOR
    var presenter: LikeInteractorOutputProtocol? { get set }
    var localDatamanager: LikeLocalDataManagerInputProtocol? { get set }
    var remoteDatamanager: LikeRemoteDataManagerInputProtocol? { get set }
    
    func getDataPokemonRamdonInteractor()
    
    func likeAction(name: String, img: String, appDelegate: AppDelegate )
    
}

protocol LikeDataManagerInputProtocol: class {
    // INTERACTOR -> DATAMANAGER
}

protocol LikeRemoteDataManagerInputProtocol: class {
    // INTERACTOR -> REMOTEDATAMANAGER
    var remoteRequestHandler: LikeRemoteDataManagerOutputProtocol? { get set }
    
    func getDataPokemonRemote(idPokemon: Int)
    
}

protocol LikeRemoteDataManagerOutputProtocol: class {
    // REMOTEDATAMANAGER -> INTERACTOR
    
    func callBackPokemonRamdonDetail(detailPokemon: DetailPokemon)
}

protocol LikeLocalDataManagerInputProtocol: class {
    // INTERACTOR -> LOCALDATAMANAGER
    
    func saveLocalLikePokemon(name: String, img: String, appDelegate: AppDelegate )
}
