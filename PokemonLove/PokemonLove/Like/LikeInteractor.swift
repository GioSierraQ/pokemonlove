//
//  LikeInteractor.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 23/07/21.
//  
//

import Foundation

class LikeInteractor: LikeInteractorInputProtocol {

    // MARK: Properties
    weak var presenter: LikeInteractorOutputProtocol?
    var localDatamanager: LikeLocalDataManagerInputProtocol?
    var remoteDatamanager: LikeRemoteDataManagerInputProtocol?

    func getDataPokemonRamdonInteractor(){
        
        remoteDatamanager?.getDataPokemonRemote(idPokemon: Int.random(in: 1..<200))

    }
    
    func likeAction(name: String, img: String, appDelegate: AppDelegate ){
        localDatamanager?.saveLocalLikePokemon(name: name, img: img, appDelegate: appDelegate)
    }

}

extension LikeInteractor: LikeRemoteDataManagerOutputProtocol {
    // TODO: Implement use case methods
    
    func callBackPokemonRamdonDetail(detailPokemon: DetailPokemon){
        
        presenter?.pushPokemonRamdonDetailPresenter(dataPokemonDetail: detailPokemon)

    }
    
}
