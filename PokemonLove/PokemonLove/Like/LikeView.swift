//
//  LikeView.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 23/07/21.
//  
//

import Foundation
import UIKit

class LikeView: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imagePokemon: UIImageView!
    
    // MARK: Properties
    var presenter: LikePresenterProtocol?
    var pokemonDetail = DetailPokemon()
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
    
        presenter?.viewDidLoad()
        
    }
    
    func viewSetup() {
        nameLabel.text = pokemonDetail.name
        
        let url = URL(string: pokemonDetail.sprites?.other?.offiArt?.front_default ?? "")
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        imagePokemon.image = UIImage(data: data!)

    }
    
    @IBAction func buttonLike(_ sender: Any) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
          return
        }
        
        presenter?.likeAction(name: pokemonDetail.name ?? "", img: pokemonDetail.sprites?.other?.offiArt?.front_default ?? "", appDelegate: appDelegate)
        presenter?.viewDidLoad()
    }
    
    @IBAction func buttonDisLike(_ sender: Any) {
        presenter?.viewDidLoad()
    }
    
    
}

extension LikeView: LikeViewProtocol {
    // TODO: implement view output methods
    
    func presenterPushPokemonRamdonDetail(dataPokemonDetail: DetailPokemon){
        pokemonDetail = dataPokemonDetail

        DispatchQueue.main.async {
            self.viewSetup()
        }
    }
    
}
