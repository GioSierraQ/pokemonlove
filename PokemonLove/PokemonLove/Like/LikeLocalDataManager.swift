//
//  LikeLocalDataManager.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 23/07/21.
//  
//

import Foundation
import CoreData

class LikeLocalDataManager:LikeLocalDataManagerInputProtocol {
    
    var pokemonLike: [NSManagedObject] = []

    func saveLocalLikePokemon(name: String, img: String, appDelegate: AppDelegate ){
        
        let managedContext = appDelegate.persistentContainer.viewContext


        let entity = NSEntityDescription.entity(forEntityName: "PokemonLike", in: managedContext)!

        let Pokemon = NSManagedObject(entity: entity, insertInto: managedContext)

        Pokemon.setValue(name, forKeyPath: "name")
        Pokemon.setValue(img, forKeyPath: "img")

        do {
            try managedContext.save()
            pokemonLike.append(Pokemon)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }

    }
    
}
