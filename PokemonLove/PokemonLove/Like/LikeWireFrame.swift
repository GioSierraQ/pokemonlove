//
//  LikeWireFrame.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 23/07/21.
//  
//

import Foundation
import UIKit

class LikeWireFrame: LikeWireFrameProtocol {

    class func createLikeModule() -> UIViewController {
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LikeView")
        if let view = viewController as? LikeView {
            let presenter: LikePresenterProtocol & LikeInteractorOutputProtocol = LikePresenter()
            let interactor: LikeInteractorInputProtocol & LikeRemoteDataManagerOutputProtocol = LikeInteractor()
            let localDataManager: LikeLocalDataManagerInputProtocol = LikeLocalDataManager()
            let remoteDataManager: LikeRemoteDataManagerInputProtocol = LikeRemoteDataManager()
            let wireFrame: LikeWireFrameProtocol = LikeWireFrame()
            
            view.presenter = presenter
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            interactor.presenter = presenter
            interactor.localDatamanager = localDataManager
            interactor.remoteDatamanager = remoteDataManager
            remoteDataManager.remoteRequestHandler = interactor
            
            return viewController
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
}
