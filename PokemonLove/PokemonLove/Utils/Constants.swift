//
//  Constants.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 22/07/21.
//

import Foundation
    
    // MARK: URL Base de server API
    let baseUrl = "https://pokeapi.co/api/v2"
    
    // MARK: Endpoint of the petitions in API
    
/**
 -/pokemon?limit=100&offset=0 -> List all pokemon by paginations
 */
    let endPointListPokemon = "/pokemon"
    
/**
 -/pokemon/name -> Details of one pokemon with name
 */
    let endPointDetailsPokemon = "/pokemon/"

/**
 -/generation -> List all generations
 */
    let endPointGeneration = "/generation"

/**
 -/generation/name -> Details of generation with name
 */
    let endPointGenerationListPokemon = "/generation"
    
