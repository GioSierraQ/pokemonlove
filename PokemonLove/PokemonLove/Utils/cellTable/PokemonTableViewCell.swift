//
//  PokemonTableViewCell.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 23/07/21.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewCell: UIView!
    
    static let identifier = String(describing: PokemonTableViewCell.self)

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: cell format configuration
    func setupCell(pokemon: pokemonSpecies) {
        titleLabel.text = pokemon.name
        viewCell.layer.cornerRadius = 10
        viewCell.layer.shadowColor = UIColor.black.cgColor
        viewCell.layer.shadowOpacity = 0.1
        viewCell.layer.shadowOffset = CGSize(width: 4, height: 4)
        viewCell.layer.shadowRadius = 5.0

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
