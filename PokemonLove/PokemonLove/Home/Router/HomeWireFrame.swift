//
//  HomeWireFrame.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 22/07/21.
//  
//

import Foundation
import UIKit

class HomeWireFrame: HomeWireFrameProtocol {

    class func createHomeModule() -> UIViewController {
        let navController = mainStoryboard.instantiateViewController(withIdentifier: "HomeView")
        if let view = navController.children.first as? HomeView {
            let presenter: HomePresenterProtocol & HomeInteractorOutputProtocol = HomePresenter()
            let interactor: HomeInteractorInputProtocol & HomeRemoteDataManagerOutputProtocol = HomeInteractor()
            let localDataManager: HomeLocalDataManagerInputProtocol = HomeLocalDataManager()
            let remoteDataManager: HomeRemoteDataManagerInputProtocol = HomeRemoteDataManager()
            let wireFrame: HomeWireFrameProtocol = HomeWireFrame()
            
            view.presenter = presenter
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            interactor.presenter = presenter
            interactor.localDatamanager = localDataManager
            interactor.remoteDatamanager = remoteDataManager
            remoteDataManager.remoteRequestHandler = interactor
            
            return navController
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    func presentNewViewDetail(from view: HomeViewProtocol, namePokemon: String){
        
        let newDetailView = DetailWireFrame.createDetailModule(with: namePokemon)
        
        if let sourceView = view as? UIViewController{
            
            sourceView.present(newDetailView, animated: true, completion: nil)
                
        }
    }
    
    func presentNewViewLike(from view: HomeViewProtocol){
        
        let newDetailView = LikeWireFrame.createLikeModule()
        
        if let sourceView = view as? UIViewController{
            
            sourceView.present(newDetailView, animated: true, completion: nil)
                
        }
    }

    
}
