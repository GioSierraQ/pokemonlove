//
//  HomePresenter.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 22/07/21.
//  
//

import Foundation

class HomePresenter  {
    
    // MARK: Properties
    weak var view: HomeViewProtocol?
    var interactor: HomeInteractorInputProtocol?
    var wireFrame: HomeWireFrameProtocol?

}

extension HomePresenter: HomePresenterProtocol {
    // TODO: implement presenter methods
    func viewDidLoad() {
        interactor?.getDataPokemon()
    }
    
    func getPokemonListPresenter(url: String) {
        interactor?.getPokemonListInteractor(url: url)
    }
    
    func showDetailView(with data: String) {
        wireFrame?.presentNewViewDetail(from: view!, namePokemon: data)
    }
    
    func showLikeView(){
        wireFrame?.presentNewViewLike(from: view!)
    }

}

extension HomePresenter: HomeInteractorOutputProtocol {
    
    func pushGenerationListPresenter(receivedData: [resultsGeneration]) {
        view?.presenterPushGenerationList(receivedData: receivedData)
    }
    
    func pushPokemonListPresenter(dataPokemonList: [pokemonSpecies]) {
        view?.presenterPushPokemonList(dataPokemonList: dataPokemonList)
    }
        
}
