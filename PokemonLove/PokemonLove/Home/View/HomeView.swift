//
//  HomeView.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 22/07/21.
//  
//

import Foundation
import UIKit

class HomeView: UIViewController {
    
    // MARK: Views
    @IBOutlet weak var tableDataPokemon: UITableView!
    @IBOutlet weak var pickerViewGeneration: UIPickerView!
    @IBOutlet weak var pickerView: UIView!
    @IBOutlet weak var viewTable: UIView!
    
    // MARK: Properties
    var presenter: HomePresenterProtocol?
    var generationList = [resultsGeneration]()
    var pokemonList = [pokemonSpecies]()
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        viewSetup()
    }
    
    func viewSetup() {
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()

        let rotationAngle: CGFloat! = -90  * (.pi/180)
        pickerViewGeneration.transform = CGAffineTransform(rotationAngle: rotationAngle)
        pickerViewGeneration.frame = CGRect(x: 0, y: 0, width: pickerView.bounds.width, height: 60)

        tableDataPokemon.tableFooterView = UIView()
        tableDataPokemon.register(UINib(nibName: PokemonTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: PokemonTableViewCell.identifier)
        
        viewTable.layer.cornerRadius = 20
        viewTable.layer.shadowRadius = 10
    }
    
    @IBAction func buttonPresenterViewLike(_ sender: Any) {
        presenter?.showLikeView()
    }
    
}

extension HomeView: HomeViewProtocol {
    
    func presenterPushGenerationList(receivedData: [resultsGeneration]) {
        generationList = receivedData
        DispatchQueue.main.async {
            self.pickerViewGeneration.reloadAllComponents()
            self.presenter?.getPokemonListPresenter(url: self.generationList[0].url)
        }
    }
    
    func presenterPushPokemonList(dataPokemonList: [pokemonSpecies]){
        pokemonList = dataPokemonList
        DispatchQueue.main.async {
            self.tableDataPokemon.reloadData()
            
        }
    }
    
}

extension HomeView: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemonList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PokemonTableViewCell.identifier, for: indexPath) as! PokemonTableViewCell
        cell.setupCell(pokemon: pokemonList[indexPath.row])

        return cell
    }
    
    
}

extension HomeView:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let name = pokemonList[indexPath.row].name
        presenter?.showDetailView(with: name)
    }
    
}

extension HomeView: UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return generationList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let modeView = UIView()
        modeView.frame = CGRect(x: 0, y: 0, width: 100, height: 60)
        let modeLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 60))
        modeLabel.text = "\(row + 1)"
        modeLabel.textAlignment = .center
        modeView.addSubview(modeLabel)
        modeView.transform = CGAffineTransform(rotationAngle: 90 * (.pi/180))
        return modeView
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        presenter?.getPokemonListPresenter(url: generationList[row].url)
    }
    
}

extension HomeView: UIPickerViewDelegate{
    
}

