//
//  HomeRemoteDataManager.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 22/07/21.
//  
//

import Foundation

class HomeRemoteDataManager:HomeRemoteDataManagerInputProtocol {
        
    var remoteRequestHandler: HomeRemoteDataManagerOutputProtocol?
    
    var listGeneration = GenerationList()
    var pokemonList = PokemonList()

    func dataListGeneration() {
         
        let session = URLSession.shared
        
        var request = URLRequest(url: URL(string: "\(baseUrl)\(endPointGeneration)")!)
        
        request.httpMethod = "GET"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("no-cache", forHTTPHeaderField: "cache-control")
        
        session.dataTask(with: request){(data, response, error) in
            guard let data = data, error == nil, let respuesta = response as? HTTPURLResponse else{
                print("Error en la conexión: \(error!)")
                return
            }
            
            if respuesta.statusCode == 200{
                
                print("Respuesta del servidor es: \(data)")
                
                do {

                    let decoder = JSONDecoder()
                    self.listGeneration = try decoder.decode(GenerationList.self, from: data)

                    self.remoteRequestHandler?.callBackGenerationsList(generationList: self.listGeneration)
                    
                } catch{
                    print("No se ha podido parsear el archivo, error: \(error.localizedDescription)")
                }
            }else{
                print("Error desconocido: \(respuesta.statusCode)")
            }
        }.resume()
        
    }
    
    func dataListPokemonGeneration(url: String) {
         
        let session = URLSession.shared
        
        var request = URLRequest(url: URL(string: "\(url)")!)
        
        request.httpMethod = "GET"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("no-cache", forHTTPHeaderField: "cache-control")
        
        session.dataTask(with: request){(data, response, error) in
            guard let data = data, error == nil, let respuesta = response as? HTTPURLResponse else{
                print("Error en la conexión: \(error!)")
                return
            }
            
            if respuesta.statusCode == 200{
                
                print("Respuesta del servidor es: \(data)")
                
                do {

                    let decoder = JSONDecoder()
                    self.pokemonList = try decoder.decode(PokemonList.self, from: data)

                    self.remoteRequestHandler?.callBackPokemonList(pokemonList: self.pokemonList)
                    
                } catch{
                    print("No se ha podido parsear el archivo, error: \(error.localizedDescription)")
                }
            }else{
                print("Error desconocido: \(respuesta.statusCode)")
            }
        }.resume()
        
    }

    
}
