//
//  HomeInteractor.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 22/07/21.
//  
//

import Foundation

class HomeInteractor: HomeInteractorInputProtocol {

    // MARK: Properties
    weak var presenter: HomeInteractorOutputProtocol?
    var localDatamanager: HomeLocalDataManagerInputProtocol?
    var remoteDatamanager: HomeRemoteDataManagerInputProtocol?
    
    
    func getDataPokemon() {
        remoteDatamanager?.dataListGeneration()
    }
    
    func getPokemonListInteractor(url: String) {
        remoteDatamanager?.dataListPokemonGeneration(url: url)
    }
    
}

extension HomeInteractor: HomeRemoteDataManagerOutputProtocol {
    // TODO: Implement use case methods
    
    func callBackGenerationsList(generationList: GenerationList) {
        presenter?.pushGenerationListPresenter(receivedData: generationList.results!)
    }
    
    func callBackPokemonList(pokemonList: PokemonList){
        presenter?.pushPokemonListPresenter(dataPokemonList: pokemonList.pokemon_species!)
    }
    
}
