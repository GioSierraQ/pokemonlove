//
//  PokemonList.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 23/07/21.
//

import Foundation

struct PokemonList: Codable {
    var pokemon_species: [pokemonSpecies]?
}

struct pokemonSpecies: Codable {
    var name: String
}
