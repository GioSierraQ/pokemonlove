//
//  GenerationList.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 22/07/21.
//

import Foundation

struct GenerationList: Codable {
    var results: [resultsGeneration]?
}

struct resultsGeneration: Codable {
    var name: String
    var url: String
}
