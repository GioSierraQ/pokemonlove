//
//  HomeProtocols.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 22/07/21.
//  
//

import Foundation
import UIKit

protocol HomeViewProtocol: class {
    // PRESENTER -> VIEW
    var presenter: HomePresenterProtocol? { get set }
    
    func presenterPushGenerationList(receivedData: [resultsGeneration])
    func presenterPushPokemonList(dataPokemonList: [pokemonSpecies])
}

protocol HomeWireFrameProtocol: class {
    // PRESENTER -> WIREFRAME
    static func createHomeModule() -> UIViewController
    
    func presentNewViewDetail(from view: HomeViewProtocol, namePokemon: String)
    func presentNewViewLike(from view: HomeViewProtocol)
}

protocol HomePresenterProtocol: class {
    // VIEW -> PRESENTER
    var view: HomeViewProtocol? { get set }
    var interactor: HomeInteractorInputProtocol? { get set }
    var wireFrame: HomeWireFrameProtocol? { get set }
    
    func viewDidLoad()
    
    func getPokemonListPresenter(url: String)
    
    func showDetailView(with data: String)
    
    func showLikeView()

}

protocol HomeInteractorOutputProtocol: class {
// INTERACTOR -> PRESENTER
    func pushGenerationListPresenter(receivedData: [resultsGeneration])
    func pushPokemonListPresenter(dataPokemonList: [pokemonSpecies])
}

protocol HomeInteractorInputProtocol: class {
    // PRESENTER -> INTERACTOR
    var presenter: HomeInteractorOutputProtocol? { get set }
    var localDatamanager: HomeLocalDataManagerInputProtocol? { get set }
    var remoteDatamanager: HomeRemoteDataManagerInputProtocol? { get set }
    
    //get data
    func getDataPokemon()
    
    func getPokemonListInteractor(url: String)
    
}

protocol HomeDataManagerInputProtocol: class {
    // INTERACTOR -> DATAMANAGER
}

protocol HomeRemoteDataManagerInputProtocol: class {
    // INTERACTOR -> REMOTEDATAMANAGER
    var remoteRequestHandler: HomeRemoteDataManagerOutputProtocol? { get set }
    
    func dataListGeneration()
    
    func dataListPokemonGeneration(url: String)
    
}

protocol HomeRemoteDataManagerOutputProtocol: class {
    // REMOTEDATAMANAGER -> INTERACTOR
    func callBackGenerationsList(generationList: GenerationList)
    func callBackPokemonList(pokemonList: PokemonList)
}

protocol HomeLocalDataManagerInputProtocol: class {
    // INTERACTOR -> LOCALDATAMANAGER
}
