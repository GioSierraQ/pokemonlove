//
//  CollectionViewCell.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 23/07/21.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageOfficial: UIImageView!

    static let identifier = String(describing: CollectionViewCell.self)

    // MARK: cell format configuration
    func setupCell(strips: arraySprits) {

        nameLabel.text = strips.name
        
        if strips.img == "" {
            imageOfficial.image = UIImage(named: "Pokebola")
        }else{
            let url = URL(string: strips.img)
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            imageOfficial.image = UIImage(data: data!)
        }

    }
    
    
    
}
