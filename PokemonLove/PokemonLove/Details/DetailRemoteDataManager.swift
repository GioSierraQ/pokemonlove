//
//  DetailRemoteDataManager.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 23/07/21.
//  
//

import Foundation

class DetailRemoteDataManager:DetailRemoteDataManagerInputProtocol {
    
    var remoteRequestHandler: DetailRemoteDataManagerOutputProtocol?
    
    var detailPokemon = DetailPokemon()
    
    func getDataPokemonRemote(namePokemon: String){
        
        let session = URLSession.shared
        
        var request = URLRequest(url: URL(string: "\(baseUrl)\(endPointDetailsPokemon)\(namePokemon)")!)
        
        request.httpMethod = "GET"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("no-cache", forHTTPHeaderField: "cache-control")
        
        session.dataTask(with: request){(data, response, error) in
            guard let data = data, error == nil, let respuesta = response as? HTTPURLResponse else{
                print("Error en la conexión: \(error!)")
                return
            }
            
            if respuesta.statusCode == 200{
                
                print("Respuesta del servidor es: \(data)")
                
                do {

                    let decoder = JSONDecoder()
                    self.detailPokemon = try decoder.decode(DetailPokemon.self, from: data)

                    self.remoteRequestHandler?.callBackPokemonDetail(detailPokemon: self.detailPokemon)
                    
                } catch{
                    print("No se ha podido parsear el archivo, error: \(error.localizedDescription)")
                }
            }else{
                print("Error desconocido: \(respuesta.statusCode)")
            }
        }.resume()

    }
}
