//
//  DetailInteractor.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 23/07/21.
//  
//

import Foundation

class DetailInteractor: DetailInteractorInputProtocol {

    // MARK: Properties
    weak var presenter: DetailInteractorOutputProtocol?
    var localDatamanager: DetailLocalDataManagerInputProtocol?
    var remoteDatamanager: DetailRemoteDataManagerInputProtocol?

    var arraySpritsInteractor = [arraySprits]()
    
    func getDataPokemonInteractor(namePokemon: String){
        remoteDatamanager?.getDataPokemonRemote(namePokemon: namePokemon)
    }
    
}

extension DetailInteractor: DetailRemoteDataManagerOutputProtocol {
    // TODO: Implement use case methods
    
    func callBackPokemonDetail(detailPokemon: DetailPokemon){
        
        
        
        arraySpritsInteractor.append(arraySprits(name: "yellow", img: detailPokemon.sprites?.versions?.generationI?.yellow?.front_default ?? ""))
        arraySpritsInteractor.append(arraySprits(name: "crystal", img: detailPokemon.sprites?.versions?.generationII?.crystal?.front_default ?? ""))
        arraySpritsInteractor.append(arraySprits(name: "emerald", img: detailPokemon.sprites?.versions?.generationIII?.emerald?.front_default ?? ""))

        presenter?.pushPokemonDetailPresenter(dataPokemonDetail: detailPokemon, arraySprits: arraySpritsInteractor)
    }
}
