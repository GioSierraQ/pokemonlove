//
//  arraySprits.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 23/07/21.
//

import Foundation

struct arraySprits {
    
    var name: String
    var img: String
    
    init(name: String, img: String) {
        self.name = name
        self.img = img
    }
}
