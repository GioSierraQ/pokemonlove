//
//  DetailPokemon.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 23/07/21.
//

import Foundation

struct DetailPokemon: Codable {
    var id: Int?
    var name: String?
    var height: Int?
    var weight: Int?
    var types: [Types]?
    var moves: [Moves]?
    var sprites: Sprites?

    
    struct Types: Codable{
        var type: Typer?
    }
    
    struct Typer: Codable{
        var name: String?
    }
    
    struct Moves: Codable{
        var move: Move?
    }
    
    struct Move: Codable{
        var name: String?
    }
    
    struct Sprites: Codable{
        var other: Other?
        var versions: Versiones?
    }
    
    struct Versiones: Codable{
        var generationI: GenI?
        var generationII: GenII?
        var generationIII: GenIII?

        enum CodingKeys: String, CodingKey{
            case generationI = "generation-i"
            case generationII = "generation-ii"
            case generationIII = "generation-iii"
        }
    }
    
    struct Other: Codable{
        var offiArt: ofiArt?
        
        enum CodingKeys: String, CodingKey{
            case offiArt = "official-artwork"
        }
    }
    
    struct ofiArt: Codable{
        var front_default: String
    }
    
    struct GenI: Codable{
        var yellow: front?
    }
    
    struct GenII: Codable{
        var gold: front?
        var crystal: front?
        var silver: front?
    }
    
    struct GenIII: Codable{
        var emerald: front?
    }
    
}

struct front: Codable{
    var front_default: String?
}
