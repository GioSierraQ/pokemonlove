//
//  DetailView.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 23/07/21.
//  
//

import Foundation
import UIKit

class DetailView: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageOfficial: UIImageView!
    @IBOutlet weak var heigthLabel: UILabel!
    @IBOutlet weak var weigthLabel: UILabel!
    @IBOutlet weak var viewDetail: UIView!
    @IBOutlet weak var viewTypes: UIView!
    @IBOutlet weak var viewMoves: UIView!
    @IBOutlet weak var collectionSprites: UICollectionView!
    
    // MARK: Properties
    var presenter: DetailPresenterProtocol?
    var pokemonDetail = DetailPokemon()
    var arrayStrips: [arraySprits]?

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    func viewSetup() {

        nameLabel.text = pokemonDetail.name ?? ""
        heigthLabel.text = "\(pokemonDetail.height ?? 0)"
        weigthLabel.text = "\(pokemonDetail.weight ?? 0)"
        
        let url = URL(string: pokemonDetail.sprites?.other?.offiArt?.front_default ?? "")
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        imageOfficial.image = UIImage(data: data!)
        
        viewDetail.layer.cornerRadius = 20
        
        createTags()
    }
    
    func createTags() {
        
        var posX = 0
        for tags in pokemonDetail.types! {
            
            let button = UIButton(frame: CGRect(x: posX, y: 0, width: 100, height: 30))
            button.backgroundColor = UIColor(red: 118/255.0, green: 163/255.0, blue: 29/255.0, alpha: 1)
            button.setTitle("\(tags.type?.name ?? "")", for: .normal)

            self.viewTypes.addSubview(button)
            button.layer.cornerRadius = 15
            posX += 105
        }
        
        posX = 0
        var countCiclo = 2
        if pokemonDetail.moves!.count == 0 {
            return
        }else if pokemonDetail.moves!.count < 3{
            countCiclo = pokemonDetail.moves?.count ?? 0
        }
        for move in  0...countCiclo {
            
            let button = UIButton(frame: CGRect(x: posX, y: 0, width: 100, height: 30))
            button.backgroundColor = UIColor(red: 118/255.0, green: 163/255.0, blue: 29/255.0, alpha: 1)
            button.setTitle("\(pokemonDetail.moves?[move].move?.name ?? "")", for: .normal)

            self.viewMoves.addSubview(button)
            button.layer.cornerRadius = 15
            posX += 105
        }
    }
    
}



extension DetailView: DetailViewProtocol {
    
    // TODO: implement view output methods
    
    func presenterPushPokemonDetail(dataPokemonDetail: DetailPokemon, arraySprits: [arraySprits]){
        pokemonDetail = dataPokemonDetail
        self.arrayStrips = arraySprits
        DispatchQueue.main.async {
            self.viewSetup()
            self.collectionSprites.reloadData()
        }
    }
    
}

extension DetailView: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayStrips?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.identifier, for: indexPath) as! CollectionViewCell
        cell.setupCell(strips: (arrayStrips?[indexPath.row])!)

        return cell
    }
    
    
}
