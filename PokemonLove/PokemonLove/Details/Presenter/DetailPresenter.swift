//
//  DetailPresenter.swift
//  PokemonLove
//
//  Created by Giovani Sierra quintero on 23/07/21.
//  
//

import Foundation

class DetailPresenter  {
    
    // MARK: Properties
    weak var view: DetailViewProtocol?
    var interactor: DetailInteractorInputProtocol?
    var wireFrame: DetailWireFrameProtocol?
    var namePokemonRecivied: String?
    

}

extension DetailPresenter: DetailPresenterProtocol {

    // TODO: implement presenter methods
    func viewDidLoad() {
        interactor?.getDataPokemonInteractor(namePokemon: namePokemonRecivied!)
    }
    
}

extension DetailPresenter: DetailInteractorOutputProtocol {
    // TODO: implement interactor output methods
    
    func pushPokemonDetailPresenter(dataPokemonDetail: DetailPokemon, arraySprits: [arraySprits]){
        view?.presenterPushPokemonDetail(dataPokemonDetail: dataPokemonDetail, arraySprits: arraySprits)
    }
    
}
