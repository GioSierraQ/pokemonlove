# README #

Informacion de la prueba realizada para Condol Labs para ingreso Desarrollador iOS - Giovani Sierra Quintero.

### What is this repository for? ###

* Una aplicacion sobre pokemon sobre arquitectura VIPER manejo de data con core data y consumo de API mediante URLSession
* 1.0

### How do I get set up? ###

* Arquitectura VIPER
* Core data
* URLSesion
* sin pod ni frameWorks

### Contribution guidelines ###

* No realice test unit
* Comente las cosas puntuales.

### Apuntes ###

* En el ejercicio no se implementaba vista para ver los pokemon a los que se dio like, lo cual no contemple en el tiempo que propuse, esta pendiente para implementarlo, lo hare en local para no afectar la calificación de la prueba hasta las 12 del medio dia.
* De resto creo que fue una gran prueba, gracias por la oportunidad y el tiempo, espero vernos pronto. un abrazo.
